package pageobject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.SeleniumHelpers;

public class CheckoutFlowPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public CheckoutFlowPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        //This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 60), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */


    @FindBy(xpath = "//a[@class='btn buy']")
    private WebElement buyNowButton;

    @FindBy(xpath = "//td[@class='input']/child::input[@value='Budi']")
    private WebElement nameEditText;

    @FindBy(xpath = "//td[@class='input']/child::input[@value='budi@utomo.com']")
    private WebElement emailEditText;

    @FindBy(xpath = "//td[@class='input']/child::input[@value='081808466410']")
    private WebElement phoneNoEditText;

    @FindBy(xpath = "//td[@class='input']/child::input[@value='Jakarta']")
    private WebElement cityEditText;

    @FindBy(xpath = "//td[@class='input']/child::textarea")
    private WebElement addressEditText;

    @FindBy(xpath = "//td[@class='input']/child::input[@value='10220']")
    private WebElement postalCodeEditText;

    @FindBy(xpath = "//div[@class='cart-checkout']")
    private WebElement checkoutButton;

    @FindBy(xpath = "//a[@class='button-main-content']")
    private WebElement continueButton;

    @FindBy(xpath = "//a[@class='list with-promo']")
    private WebElement creditCardButton;

    @FindBy(xpath = "//input[@name='cardnumber']")
    private WebElement cardNumberEditText;

    @FindBy(xpath = "//input[@placeholder='MM / YY']")
    private WebElement expiryDateEditText;

    @FindBy(xpath = "//input[@placeholder='123']")
    private WebElement cvvEditText;

    @FindBy(xpath = "//a[@class='button-main-content']")
    private WebElement payNowButton;

    /**
     * Click On Buy Now Button
     */
    public void ClickOnBuyNowButton() throws InterruptedException {
        selenium.clickOn(buyNowButton);
    }

    /**
     * Enter Name
     *
     * @param name
     */

    public void enterName(String name) {
        selenium.enterText(nameEditText, name, true);
    }

    /**
     * Enter Email
     *
     * @param email
     */

    public void enterEmail(String email) {
        selenium.enterText(emailEditText, email, true);
    }

    /**
     * Enter Phone No
     *
     * @param phoneNo
     */

    public void enterPhoneNo(String phoneNo) {
        selenium.enterText(phoneNoEditText, phoneNo, true);
    }

    /**
     * Enter City
     *
     * @param city
     */

    public void enterCity(String city) {
        selenium.enterText(cityEditText, city, true);
    }

    /**
     * Enter Address
     *
     * @param address
     */

    public void enterAddress(String address) {
        selenium.enterText(addressEditText, address, true);
    }

    /**
     * Enter Postal Code
     *
     * @param postalCode
     */

    public void enterPostalCode(String postalCode) {
        selenium.enterText(postalCodeEditText, postalCode, true);
    }

    /**
     * Click On Checkout Button
     *
     * @throws InterruptedException
     */
    public void clickOnCheckoutButton() throws InterruptedException {
        selenium.clickOn(checkoutButton);
    }

    /**
     * Click On Continue Button
     *
     * @throws InterruptedException
     */
    public void clickOnContinueButton() throws InterruptedException {
        selenium.clickOn(continueButton);
    }

    /**
     * Click On Credit Card Button
     *
     * @throws InterruptedException
     */
    public void clickOnCreditCardButton() throws InterruptedException {
        selenium.clickOn(creditCardButton);
    }

    /**
     * Enter Card Number
     *
     * @param cardNumber
     */

    public void enterCardNumber(String cardNumber) {
        selenium.enterText(cardNumberEditText, cardNumber, true);
    }

    /**
     * Enter Expiry Date Card
     *
     * @param expiryDate
     */
    public void enterExpiryDate(String expiryDate) {
        selenium.enterText(expiryDateEditText, expiryDate, true);
    }

    /**
     * Enter CVV Card
     *
     * @param cvv
     */
    public void enterCvv(String cvv) {
        selenium.enterText(cvvEditText, cvv, true);
    }

    /**
     * Click On Pay Now Button
     *
     * @throws InterruptedException
     */
    public void clickOnPayNowButton() throws InterruptedException {
        selenium.clickOn(payNowButton);
    }
}
